# CCShortTV

#### 介绍
使用ArtTS语言，API9以上，HarmonyOS系统的短剧开源代码，使用GSYVideoPlayer作为核心播放器的小短剧。

#### 软件架构
主要以ArkTS，ArkUI编写为主，拥有市面上的短剧页面。

1.  推荐页面，主要随机推荐热门的小短剧并播放
2.  短剧页面，主要是对短剧类型进行了分类，可供选择。
3.  收藏界面，主要是对自己感兴趣的短剧进行收藏。
4. 我的界面，主要是用户信息展示。


#### 安装教程
1.git clone 项目地址

#### 使用说明
目前正在代码编写中

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
